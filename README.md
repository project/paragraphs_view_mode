# Paragraphs View Mode (paragraphs_view_mode)
This module is designed to make paragraphs view mode selections available in paragraph forms by adding a 
view mode selection field. Available view modes for each bundle are configurable in the view mode selection widget,
and options are limited to only bundle configured view modes.


## Installation

### 1. Composer

### 2. Git

### 3. Download

## Usage

### 1. Configuring View Modes

### 2. Limiting View Modes
